---
layout: post
title: Software Snapshots
date: 2017-10-30
---
In general, software is not as rigid as other things that we build, it requires constant maintenance just to keep functioning. I am willing to wager that most of the software that has been built is already broken. Because everything else we build does rely on other services i.e. cars rely on petrol to work at all. However, software is unique though.

In the previous example, the petrol industry is reliable, as long as cars have existed, they have always be there. And we act as like they will always exist in the future. People who build cars don't have to take into account the fact that one day petrol will not be available and thus they would have to build another fuel system in the car just in case they would have to use it.

And in some regards, software is no different, to build complex software, it would have to be on top of existing platforms and services (which are software themselves). A lot of software relies on services that are free or cost a cheap fee. Thus there is not an incentive from content providers to keep these services always up and running and more importantly, legacy compatible. For example, if a user/password authentication service decides that they need update their security and not allow legacy compatibility then every software that relies on that service and is not currently in maintenance will go down.

The difference between how we build software and other things. Is that for the first time in our history, we managed to build something that is flexible, that can be updated and changed without a "recall". However, it is far too common to underestimate how hard and expensive it is to maintain and update code. Which is understandable, since it took humanity thousands of years of building to accumulate the level of knowledge that allows us to build a skyscraper nowadays. Coding on the other hand, we just have not had the time to develop coding standards and practices the same way that other industries and for that reason, our method of building code is not as rigid as our method of building buildings.

I think that digital libraries and sources of information are superior to the "old school" ones in many ways. However, one of the ways that the are lacking is with reliability. And it doesn't take much to notice that. The information that is in a book will always be in the book, the book might not be accessible, but the information is safe within it. However, with software, that is not the case. Take for example, any forum or thread/post that is on the internet from let's say before 2012. Unless it is pure text, there probably is either a broken link or image in the thread, often rendering it useless.

Furthermore, imagine with me what will happen if one of the major sources of data on the internet e.g. (social media site or cloud services) goes down. And inevitably, they will all go down, then a huge source of human history will be lost with it.

Lastly, I hope that in the future, there will exist a way for us to snapshot a software in its current state. That is to say a snapshot that is more than just an archive, but a fully functioning snapshot even after the APIs and other services are down. Off course, I don't even know how to begin to approach this problem, but I still think it is critical that we work on it asap.

In summary, unlike other things that we build, software is vitally dependent on a whole bunch of other things that are constantly changing. And to preserve the knowledge and history that we perhaps only record with software, we need to develop "bubble" sort of environments that preserve the state of software and all of it's dependencies.
