---
layout: post
title: Spiderman in Sunnyside (Part 4)
date: 2024-09-3
---

Continuing from parts [1](https://saleh.soy/spider-man-in-sunnyside-part-1) & [2](https://saleh.soy/spider-man-in-sunnyside-part-2) & [2](https://saleh.soy/spider-man-in-sunnyside-part-3).

In this post, We'll go over Sam Raimi's (aka Tobey McGuire's) trilogy:
* 2002's [Spider-Man](https://en.wikipedia.org/wiki/Spider-Man_(2002_film))
* 2004's [Spider-Man 2](https://en.wikipedia.org/wiki/Spider-Man_2)
* 2007's [Spider-Man 3](https://en.wikipedia.org/wiki/Spider-Man_3)

We'll also swing by the animated Spiderverse films:
* 2018's [Spider-Man: Into the Spider-Verse](https://en.wikipedia.org/wiki/Spider-Man:_Into_the_Spider-Verse)
* 2023's [Spider-Man: Across the Spider-Verse](https://en.wikipedia.org/wiki/Spider-Man:_Across_the_Spider-Verse)

And we'll also talk about the excellent Spiderman games made by insomniac.
* 2018's [Marvel's Spider-Man](https://en.wikipedia.org/wiki/Spider-Man_(2018_video_game)).
* 2023's [Marvel's Spider-Man 2](https://en.wikipedia.org/wiki/Spider-Man_2_(2023_video_game)).


## Spider-Man (2002)

* Scene on 41st st.
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_midtown_a.jpeg" style="width:80%">

* At the end of the movie the fight scene is on the Queensboro bridge which connects Manhattan to Queens.


## Spider-Man 2 (2004)

* lorem ipsum

## Spider-Man 3 (2007)

* lorem ipsum

## Spider-Man: Across the Spider-Verse (2023)

* Burger King in Astoria (PIC)


## Marvel's Spider-Man 2 (2023)

* Game footage in Queens and the east river baby !!
https://www.youtube.com/watch?v=XrPZSq5YXqc
https://old.reddit.com/r/PS5/comments/13qxvph/marvels_spiderman_2_gameplay_reveal/jlhahez/