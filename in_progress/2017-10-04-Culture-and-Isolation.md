---
layout: post
title: Culture and Isolation
date: 2017-10-04
---

I think that there must be a positive correlation between how complex a culture/language is and how isolated it is. I think that this is a point that most will agree with partially if not totally. However, I'd like to argue that the inverse is also correct, the more open and spread a culture is, the more it must be simple.

I think that an easy example for me to induce this idea is to compare and contrast the Arabic and English languages. Arabic is so complex, mainly because of how isolated geographically close regions of the Arab world have historically been and still are. While English is spoken all over the world, yet everyone who speaks it is connected to where the language originated from (US/UK).

Furthermore, when a culture/language is so spread over large areas/peoples, I think that it is almost impossible for said language/culture to be complex, since the need to keep a constant relation between all the speakers stops it from evolving uniquely in a certain region.

Now, I think this hypothesis leads to a very interesting point of discussion. I have heard a lot, especially from young people, usually europeans saying that "Americans have no culture" (To clarify, Americans in this context meaning people from the US). Maybe it is where I hang out and whom I spend time with, but it seems more and more common, to hear those words, sometimes, even from some Americans.

And I strongly disagree with that sentiment, Americans do have a culture, a very beautiful and rich one, especially when considering how young the country is! Everywhere one can go in the world, a trace of American culture can be found. However, I think that the lack of complexity in American culture that many sense, is a bias fallacy. Since, it is because of how spread American culture is, that one might feel like they know so much about it, maybe without even without having visited or met many Americans, and that might give them the perception that it is a "shallow" culture.

For example, usually when I hear a joke in America, I feel like it is easily translatable to Arabic without the need to fill the listener up on background info on why this is a "joke". However, that is almost never the case in Arabic. And that isn't because arabs are more "cultured" than Americans. But because of how spread American culture is, and how isolated (in comparison) Arab cultures are.
