---
layout: post
title: Longest Block in the World
date: 2018-04-17
---

A lot of the time, it seems that the two blocks from my subway station to my apt are the two longest blocks in all of NYC. And I know that I usually walk them late at night, when I'm exhausted and the weather maybe (probably?) sucks and that affects my judgement. But even being aware of my bias doesn't change how long and draggy they feel. One time I measured them (using my steps, I figured why use somebody else's feet when I can use mine). And they are longer than average, but not the longest for sure.

Similarly, when I lived on Larry ln in Austin, my house was on top of the (mini) hill. And biking home, I would blaze through the hills of Austin until I got to the hill on Larry ln, which felt harder than the rest of the bike ride (which it wasn't).

And so I was thinking about these feelings, and this story is just a small example of how hard it is to measure things accurately without putting a great deal of effort into trying to do so.

Some time ago, this fact kinda bothered me, that my judgement was hindered by outside forces that I, a lot of the time had no control over. But with time, I learned to accept this and be content with trying my best to be aware of these biases.
