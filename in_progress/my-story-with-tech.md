---
layout: post
title: My Story with Tech
date: 2018-07-04
---

All my work experiences before Kiva have been jobs in the field. And I really enjoy that kind of work. I like being in the front line, dealing with people and getting things done. It's a great opportunity to develop soft skills, and simply, it makes me feel more human by connecting with other people. Through working in the field, I have never had a shortage of diverse friendships, people to chill with, people to eat with, people to play soccer with and everything in between.

Helping people, especially those less privileged is a noble thing, it makes me very happy doing that kind of work. Not just because it feels good, but more importantly, because being a contributing member of society really matters to me. And I want to live in a community where people stand by one another and help/support each other. And so I try to start with myself. In Islam, there's a [saying](https://quranenhadith.wordpress.com/2013/05/07/165/): "The most beloved people to god are those who are most beneficial to the people".

However, as much as I love working in the field, it still left me wanting more work responsibilities. I knew that I had useful skills that I was not utilizing at such jobs. Also while the type of impact those kind of jobs had was amazing, I literally had a hand in changing a handful of peoples' lives for the better. But it took such a long time and so much effort to achieve that. I wanted to do something in my life with a similar impact but with more reach. And that's where tech comes in.

In my life, I've travelled and spent extensive time in Latin America, Europe and North Africa. Finally settling in Texas to pursue a Bachelor’s degree in Computer Science. Having seen such a diversity of lives, values, and ways of being has drastically shaped my understanding of the world.

Traveling has not only shown me how important it is to open up to people, but also exposed me to sides of life I had never seen before, including extreme poverty. Nothing has shaped my life experiences as much as seeing how unfair and unjust poverty is in many places in the world. In the same vein, I realize now that the way to try and bridge the gap between the most and least privileged in the world is not by individual acts of charity, but a more nuanced approach.

An important part of this process is by implementing systems that help the less privileged to overcome the hardships that most people in the world would never have to confront. In addition to these systemic changes, it seems necessary to bridge the gulfs of inequity through the development of technology. This particular aspect is what I wish to dedicate my life to because I believe that increasing access to information and ability is one of the most efficient, unbiased, and universal ways to make what was once unattainable within reach for everyone.

And that's my story with tech, that's why I studied and dedicated so much of my time to Computer Science. Because I believe that developing my skills and becoming a good software engineer is the best way for me to help the world be a better place.

✌️
