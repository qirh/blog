---
layout: post
title: Languages and Empathy
date: 2015-04-08
---

I had always dreamed that if all people could speak the same language, there would cease to be conflict. Whenever I meet Hebrew or Persian speakers, one of the first things that must come up, are the conflicts that exists between speakers of said language and arabs. I would always voice my (at the time) conviction that if we took the time to learn each others' languages, we would just understand each other, and have empathy for one another and peace would ensue after that.

Out of the 10s (100s?) of people that I said that to, almost all unanimously agreed. All except one, who brought up the point that people, belonging to the same culture/language get into conflicts all the time. Moreover, while in softer words, he said that while this concept of learning languages as a way of fixing the world is just pure childish.

I both agree and disagree with that view. I agree because it's true, people of the same culture/language fight amongst each other all the time. But I disagree because I think that learning a language is not the same as being raised with it. You would start seeing things in a different light than native speakers.

But regardless, I really do believe that the reason conflict exists in the world is lack of empathy. And like most things in life, it's a really complex issue. That goes beyond the simple explanation that if people shared the same language they wouldn't fight.

Sharing a language with someone will probably positively correlate to their empathetic relationship, at least in my case it has. However, as much as I wish it, most unfortunately, I don't believe that there exist, by necessity a positive correlation relationship between language acquisition and empathy.
