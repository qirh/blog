---
layout: post
title: Learning English as a Native Arabic Speaker
date: 2017-08-15
---

A lot of times, when someone speaks a second language, it's easy to deduce what their first language is. For instance, I could tell a German speaking English from a Francophone speaking English from a mile away (as long as they have accents of course).

The interesting thing about the Arabic language, is that not only is it easy to tell an Arab when they speak English. But it is also not that hard to tell which part of the Arab world they're from, since different Arabs speak with different and distinct dialects, that affects how they pronounce things in English and also how their mannerisms of speech. (of course, it all depends on how strong the accent is)

Now what I have been wondering, is that if in the same vain of speakers of a common first language speaking a second language with a similar figure of speech, would they also write that second language also similarly.

I began with myself and compared how I speak and write English as a non-native speaker/writer to my Arab peers. I don't think that I fall into many of the mistakes common for Arabs when they speak English (Pronouncing P as B, Pronouncing 'th' as Z).

However, I do feel like that Arabic affects how I write in English and especially in the case of writing run on sentences.

Speaking of run on sentences!! I thought it was just Arabic speakers that did that, but no, Hebrew speakers do as well, and I wonder if native speakers of all semitic languages have this problem. Although I have had very little exposure to Maltese speakers' writings in English seem to write the way a native latin-root language speaker would write.

Lastly, I don't know if Tigrinya and Amharic are considered semitic, but they both sound really beautiful and I wish one day to learn them :)

update, 2018: This [study](http://www.nadasisland.com/languageacq-erroranalysis.html) is amazing
