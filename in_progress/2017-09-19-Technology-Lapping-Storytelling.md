---
layout: post
title: Technology Lapping Storytelling
date: 2017-09-19
---

Technology is developing faster than we can cope with, our cities are not built to withstand change and neither are we. However, we have been adopting well, and although not as well as us, but the major metropolitan areas have been adopting to new technologies as well.

However, and I have absolutely no evidence or point of reference for this claim, but I think that writers, novelists and storytellers in general have been having the hardest time trying to develop their methods. To this day, many stories have to make up weird excuses for creating a situation where modern technology cannot be utilized. In the same vain, the DIY fix approach to push narrative forward is simply a relic of the past. It is highly unlikely, almost impossible to DIY fix a phone these while stranded in an island.

However, I understand how hard it is for storytellers to try to keep up with how fast technology is developing. Basically, never in history have storytellers had to change so many aspects of their storytelling to fit the current situation in such a little amount of time. The changes that could drive a whole narrative happen every 3-5 years now, while before, generally, in writing history, I wouldn't think a storyteller would have had many opportunities in their life to witness storytelling-changing technological breakthroughs.
