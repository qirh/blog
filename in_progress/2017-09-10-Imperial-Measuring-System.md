---
layout: post
title: Imperial Measuring System
date: 2017-09-10
---

I agree that the metric system makes more sense and is easier to use and that it deserves to be the global standard. However, I also think that there is room for multiple measurement systems in our daily lives.

I don't think it's fair for people to ask that we only use one measurement system. In fact, I think that people can easily think in multiple measurement systems. Same as how people who drive their vehicles during the day, usually find no problems driving them at night, although it's a significantly different environment.

Although I don't have evidence to support this, but I think that the difficulty people usually have when thinking in two different measurement systems 100% placebo effect and not because of actual mental constraints.

Take for any instance anyone who travels frequently between different measurement system regions or people whose careers dictate they learn multiple systems, they all never have problems navigating them.

Moreover, I think that when a person learns to simultaneously utilize different measurement systems. They develop an intuition and a deeper knowledge on the measurements/sizes of things. Same to how learning a foreign language can give the person insight into their native language from a different perspective. Meaning that, when done well, diversity in measurement systems will have positive effects on a community. As it broaden their (measurement) horizons.

Looking into the future, I think the areas of the worlds that still use the empirical system should still keep using it for daily life use but at the same time emphasize learning about the metric system for any measurement that requires precision, science for example.
