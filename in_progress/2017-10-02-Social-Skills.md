---
layout: post
title: Social Skills
date: 2017-10-02
---

I think that in the conversation about the injustices that minorities face in society(es) there exist a lot of people who are forgotten or maybe greatly under-represented. Some of those people, are the ones who have full control of their mental and physical facilities, yet, they lack the social skills to navigate the every once and a while problems that occur to everyone, i.e awkward af.

For instance, let's assume a person comes from what is perceived as a privileged class of society, and that said person is awkward and doesn't know how to talk to people. Moreover, because of not knowing how to express their desires and wishes and opinions in a concise, delightful manner. They might not have the opportunity to show how smart, nice or caring they are, simply because they don't have the capabilities to express them. While, if first knowing/meeting them, they might seem dumb or cold. Upon further knowing them however, one might find that they are really smart and/or have a really good heart.

Thus, when people don't have the capability to establish social relations easily or be able to express their opinions and beliefs. I think that they deserve to be given be given protections by the law and accommodations by society.
