---
layout: post
title: Skyscraper Therapy
date: 2021-01-12
---
I'm writing this having just come back from a wonderful and amazing (albeit emotionally intense) trip to Ecuador.

<div class="linebreak"></div>

##### Preface
Before moving to NYC I used to travel a lot. However, NYC has a way of swallowing up time and I have not travelled (outside the US/Saudi) since I moved here. And so travelling isn't something that I'm not used to, but it's been so long since I travelled abroad that I have forgotten how to travel without even realizing it.

<div class="linebreak"></div>

##### Back to Ecuador
The whole trip was super rushed and last minute. I realized that I had time off only a few days before my trip and made a quick decision to go to Ecuador, the reason I picked Ecuador is a whole other story but basically, I randomly met two groups of Ecuadorians and they encouraged me to go :) Encounters like these are some of my fav part about being in NYC!

I did not feel much in the few days leading to the trip. I have been busy with work and a few other commitments that I almost forgot that I'm flying out in a few days. However, on the day of the trip, I started feeling anxiety and stress, partly because I was incredibly busy that day and that I had not packed or made any preparations for the trip, but mostly because I have not travelled in so long that I was afraid to travel again.

I would say that this trip has been one of the most memorable trips I have taken. To be honest, nothing about this trip has been that unique. Everything I've experienced on this trip I have done so before in other trips. I've climbed really big mountains and met really incredible people before. But unlike before when I did 2-4 big trips a year. This is my first trip in almost 3 years! And throughout the whole trip, it felt like I have been travelling for the first time. Like I have re-discovered the feeling and sense of wonder that I was so used to feeling before.

By the second day of my trip, I was feeling a travel-high that I have not felt in such a long time. Life was so exciting and eventful that within a few days, the idea of returning to a somewhat routine daily life in NYC was so far removed from my mind that sometimes I felt like I have been travelling my whole life and not just for a day and a half haha.

The trip was amazing. I had such a wonderful time. For 3 weeks in Ecuador, I encountered some of the most genuine and amazing people that I have ever known, and the vast majority of them by pure coincidence. Here I feel a bit of despair trying to put into words what it felt like for me being there. Not everyday was exciting and not everyday was particularly interesting. But everyday, I don't know how to describe it, I felt in closer touch with my humanity. And maybe it felt close to the feeling of coming back home after a very long and tiresome journey, but also there is a bit of melancholy aspect to it as well, being in a faraway land, where I didn't know anybody or barely anything and still felt more home than where I live or where I'm from.

Towards the last week of the trip, some incredibly intense and stressful things were happening. No need to talk about specifics, everything is much better now :) What matters is that it was a really hard time and that I had to push back my flight and miss a few days of work. It was an emotionally exhausting time.

On the last day of my trip (in which I thought I might have to push my flight back again, but thankfully didn't have to). It took >24 hours, and many means of transportation to get back to my apartment in NYC. It was a hard journey all over. I was so tired from lack of sleep and general exhaustion (I spent my last day before starting the trip hiking in a national park). And also felt overwhelmed by so many emotions.

<div class="linebreak"></div>

##### We are often at our most vulnerable when, after a hard journey, the end is in sight

My last flight coming back home was from Chicago to NYC. As the plane started taking off, I felt so many things that culminated into an irresistible urge to cry. I was too tired though and did not cry, I was completely worn out. Before moving to NYC, I used to have shame about things like crying in public, however after crying in a crowded NYC subway I just don't care anymore, I can cry anywhere in the world haha. Thanks 🗽🚇.

I think the closest words that I can use to describe my feelings during a huge part of that day and this last flight specifically, is a deep sense of grief and loss. Che Guevara said: *yo, no soy yo; por lo menos no soy, el mismo yo interior.*<sup>[\*](#nota)</sup> And that felt like a transition period between the old self and the new self, a new becoming, a funeral for the past, for the adventure and the freedom and the spontaneity.

Although I am **very** happy in NYC, at those moments, I could not imagine being content with living a settled/routine/work-at-a-desk-job life again. And so I sat in silence looking out the window, mourning the life that I (briefly) lived while travelling, and fearing a future where I feel trapped/stuck in a life that isn't interesting or fun or exciting. There's an endless and continuous spread of thin clouds beneath the aeroplane. Wow, even the weather sucks today ha!

The plane got closer and closer to NYC and I sat motionless, thoughtless, emotionless, energyless. Things that I almost never am. I did not even have the energy to open up my phone and do some reading or take a nap. The plane descended under the clouds and I started to see the landscape, we pass a bunch of towns and small-medium cities, we pass one with a river and some tall buildings, that must be Newark I thought (it wasn't haha). A little bit after that I saw the first piece of land that I knew for sure that I recognised, Staten Island! I recognized the green heart of the island that I love so much.

And then a little bit after that, the NYC skyline started appearing. It looked like a magnificent forest of steel and rock and glass. I was so tired and felt so bad I could not imagine anything could lift my spirits up at that time. But seeing that skyline did exactly that! It was almost like all of a sudden the doubts and second-guessing that had built up inside me about coming back instantly evaporated at the beautiful sight of this city. I didn't even feel tired anymore, I felt euphoric!

I don't think NYC is a place that I can live in long term, it's just too much, the mere idea of growing old in the city scares me so much, i'm barely making it now how would I survive when I'm old haha. But also I just don't know where to go, I have never cherished a place as much as I do this city. Very often I would be biking/walking/just existing around the city and I witness events/vistas/conversations and I just have to pinch myself, can this be actually happening? how is this even my quotidian life? how can this place be so profoundly contradictory?? is there anywhere else in the ~~US~~ planet like this??? and most importantly, are other members of the human race capable of keeping up with our walking pace??? is there even anything worth anything outside the 5 boroughs?????

Straight up, way too often it feels like I'm living days/nights directly out of [After Hours](https://letterboxd.com/film/after-hours/) (a great movie if you haven't seen it!).

NYC and I, probably isn't the healthiest relationship, let's just say it's complicated. Living here definitely is not easy. Yes sometimes it can be easy, but honestly this is the hardest place I have ever been to. I've had some of the most challenging times in my life here. NYC by all means is not a healthy place for me to be. It's really stressful and there's always too much going on. But on the other hand, I am happy here, very very happy!! I guess there's a reason Gil-Scott Heron sung [NY is killing me](https://www.youtube.com/watch?v=recCXTgAHQ4).

Yet regardless of that, I just can't imagine leaving the city. I don't think I'm capable of living anywhere else. I have this intense and inexplicable feeling of attachment to the city. And it feels so crazy for me to say this. Because this city is so prolifically insane. Sometimes, as soon as I leave the city, I immediately feel a sense of longing. And it's really weird. I literally would be in some of the prettiest places I have ever been in and my only thought is eeh it's nice but it's not new york. But I guess that's not a unique sentiment, [frank o'hara said](https://www.poetryfoundation.org/poetrymagazine/poems/26538/meditations-in-an-emergency) `I can’t even enjoy a blade of grass unless I know there’s a subway handy, or a record store or some other sign that people do not totally regret life`. And I do agree with him, after experiencing new york, I expect more from life. And the truth is that nowhere else compares. So I guess I'll add addiction to the city to the list of 3000 other maladies/heartbreaks/injuries that this city has inflicted on me 🤣

Everybody already tells me this, but I was always hesitant to accept it, but I now for certain know that I'm a new yorker. I'm not American, but I'm a new yorker, and it doesn't matter where I end up, I'll always be a new yorker.

I am still recuperating from this trip physically and emotionally and mentally and spiritually and everything haha. But I have no doubt in my mind that New York is my home. But I also know for certain that I want to escape NYC more often to discover new places far away. And then, I'm going to come back to this beautiful and amazing and gorgeous city that I feel so so lucky to call home.

<a name="nota">\*</a> I am, no longer me; at least I am no longer the same person I once was
