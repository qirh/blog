---
layout: post
title: Poor Documentation
date: 2015-08-20
---

As a Computer Science student, I have to use a lot of different tools and technologies for a relatively short period of time. Then move on, and use a new set of tools and technologies (which are usually new to me, at least in my case). Oftentimes, I would have to juggle multiple development environments each with specific convention/styles at the same time. Which I wouldn't imagine is out of the ordinary, especially for a developer in the industry.

Moreover, I feel that in CS, especially in a university setting the assignments/projects are not bound by time, there is almost always some things to add or to refine, at least, one can always write more tests. I expect whenever teams collaborate on a project, they would agree on using a set of style/conventions, or at least they should. Thus, I am not complaining about being forced to code in a style and using tools that I would prefer not to use. However, I think that having to switch between these tools and styles depending on the instructor and course, can have a bad impact on the student, by limiting their creativity, in a period of time where they are discovering the many different ways to approach problem-solving.

Now the problem is that when a developer is learning a technology and knows just barely enough to get their project working properly. They might not be able to write good documentation. I have always had that problem. Oftentimes, when trying to work with an environment, I approach that environment the same way I would approach a similar one that I am comfortable with. For example environments like Java and C#. And of course, this method of learning extends to other aspects of life, as most things in life are just nicer versions of other things that exist.

Finally, going back to some of my older projects, I didn't like the way I documented things, so, this summer, I set to change it. I have found two very good ways to approach making code more understandable. One, credit all the sources that I used to fix problems, whether I use code directly from there or am inspired by a solution. After doing this step, I'm embarrassed to see how much I rely on stack overflow haha. Secondly, include the design document in the project files. I, as I assume most other developers as well, usually start the project with scrambling on a piece of paper the main structure of how I envision the program will be, I never really paid attention to these scraps of paper, and usually threw them out after building the abstract structure into actual hard code that runs. However, I have tried to include a design doc of sort, usually, it's not the same as what I started with since the abstract structure usually changes in the middle of the project for performance/security reasons.

happy documenting
