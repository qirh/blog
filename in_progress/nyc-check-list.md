---
layout: post
title: NYC Checklist
date: 2018-03-19
---

**updated**: 2019-02-11

---

### my nyc checklist
- [x] get a standing desk at work. **update** i don't sit anymore haha
- [x] learn how to sleep through the noise. **update** i can't sleep without sirens anymore
- [x] don't replace yall with you guys. **update** there's a lot of peer pressure, but yall is here to stay!
- [x] learn to navigate the city so well that i can answer tourists when they ask for directions. **update** i know nyc so incredibly well now i surprise myself sometimes. **update x2** i'm amazed at just how big the city is!! i met a person who told me they lived in `sutton place`. the name sounded familiar but i didn't know where that is. which is really cool because while i do know most of the city very very well, here is an example of a neighbourhood which is super close to where i live and that i have passed through so many times. but i just didn't know what it was called. there's always more to learn before finally starting my tour guide business :)
- [x] catch the train while the door is closing. **update** pfft i've caught trains after the door has closed 👀
- [x] be a really really fast walker. **update** 🏃‍🏃‍🏃‍ <-- me in my lunch break, actually me anywhere anytime
- [x] don't get robbed. **update** nothing so far :) but honestly, the city is really safe!
- [x] visit all 5 bourughous. **update** biked all of them in the first week :)
    - [ ] learn how to actually spell borough. **update** i give up it's too hard
- [x] visit all (the big) museums. **update** done :) my favourite is the museum of the american indian which right outside of work
- [x] see a (the?) [coyote](https://www.villagevoice.com/2016/10/12/how-coyotes-conquered-new-york/) **update** saw it twice in central park


---

- [ ] don't turn into [fran lebowitz](https://www.youtube.com/watch?t=143&v=3W2tI4-5Tb4). **update**: ok i don't know why i was afraid of her, honestly she is soooo cool. we're very (very) very different people her and i. BUT i do feel like i am slowly turning into her, which is not a problem at all because i've turned around and now i'm a huge fan (fran if you one day read this, this is for you 🌹). anyways if i stay here for like 10 years i think i'll be 100% fran haha
- [ ] don't eat too much.  **update**: woops i eat way more than any other period of my life but the food here is soooo good it's hard to resist. but i also bike like 40 miles a day on average so it balances out ha!
- [ ] pick up at least a bit of the new york accent (failed to accomplish this in texas). **update** not only did i fail to pick up anything but also the funny thing is that ever since moving here my old friends have been telling me that my arabic/english has gotten worse haha!
- [ ] get featured on [@hotdudesreading](https://www.instagram.com/hotdudesreading/) **update** ok so I don't think I ever did make it, I don't have instagram but I asked a few friends to keep an eye out for me, actually this one I really really wanted haha, but in my defence, i almost never take the subway unless i'm with someone
- [ ] take a language class from a part of the world I know nothing about (maybe mandarin, hindi or haitian). **update** never had time honestly, but i did learn some farsi
- [ ] be the first person to cross the [manhattan bridge](https://nyc.streetsblog.org/2018/07/02/eyes-on-the-street-say-hello-to-nycs-first-bike-counter-at-the-base-of-the-manhattan-bridge/) in any day **update** i wrote this one a few months after all the other ones. the bike counter is on the manhattan side of the bridge which is convenient because that's where i live now! i've crossed the bridge so many times close to midnight but i've never been #1. there was once a time i forgot honestly what number i got exactly but i think it was <20, maybe even <10. we'll see maybe this is something that will happen in the future :) but in the meantime, i found a temple in chinatown on my way home after crossing this bridge that has a cat that is super cute and playful so i stop to say hi (through the glass window) every time i pass by 🐈


---

### places I've cried in
#### ~~bourough~~ borough
- [x] manhattan
- [x] brooklyn
- [x] queens
- [ ] bronx
- [ ] staten island, lol does anybody even cry in SI?

#### also
- [x] on the subway
- [x] riding the bus
- [x] riding my bike (although to be fair i literally do everything riding my bike so this sorta goes without saying)
- [x] walking (usually back home haha)
- [x] times square
- [x] williamsburg bridge (to be honest i think I have experienced my entire emotional range on this bridge)
- [ ] i'm sure i'm missing a few this is just what I remember at the moment

---

### things i've run over on my bicycle
i'm a very careful and cautious cyclist but sometimes things happen
- a pigeon (rip) eating on the bike lane on 6th ave
- dead rats (i'm not entirely sure, but i don't think any were alive before coming in contact with the bicycle)
- a squirrel (this one i'm really sad about)
- a lady's foot.

---

### other random notes
- i am genuinely shocked at the egregious amount of people who are out of their minds. what's happening here that drives people insane? **update** yup, [this is me in nyc](https://science.sciencemag.org/content/179/4070/250)
- almost every day or two i feel like i overhear conversations or see people that i'm just like wow this is crazy, i can't believe i'm seeing/hearing this, i should write it down, this is insane, and i haven't written anything down yet and i forgot most of these interactions. BUT there's honestly an endless supply of them in NYC haha
- nyc is a very peculiar place, it's very contradictory, it's a city of millions of people, literally people stacked on top of each other and yet there is such an air of solitude and seclusion. i love it though because i'm a people person who loves to be alone :)
- the spanish spoken here is so so varied, almost everyday i'm learning new slang and new expressions from all over the hispanic world! i've never heard such a mix of dialects/accents of spanish before. it's amazing, and what's even more incredible is that they always try to guess which hispanic country i'm from haha
- just an example of the daily life in nyc, the very sweet and lovely ladies working in the laundromat are teaching me mixteco and i can't even put into words the feeling of 🤯 🤯 that i feel. the fact that i don't even have to go out of my way to have a window into a language/culture/life of a person who's first language is mixteco and that this is just what i do while washing my clothes is incredible
- nyc can be a very very tough place. one thing that makes it easier to live here is having good neighbours. i thankfully have had great neighbours everywhere i've lived here! even before moving here, i've always had good relationships with my neighbours. but before nyc i've never really had to rely on my neighbours for things, sure i've asked them for favours here and there but i just never needed more than that (and neither did they). but in nyc, i really rely on my neighbours for basic things (and vice versa). and these things can be very small, like for instance, i only have two chairs (there's no space in the apt for more), but i often need more than two and so i often borrow my neighbour's chairs. and another neighbour i carry some things for her up/down the stairs because it's hard for her, and here and there there's always things like that, it's very nice and helpful, and feels like we live in a community. i never thought that i would move to nyc then just get to know so many neighbours and we would help each other out, the bad stereotypes about people in this city were really off!
