---
layout: post
title: Why I Don't Have Social Media
date: 2016-10-12
---

I think that social media is the worst thing that happened to the internet.

The internet allows people from all different backgrounds to connect and share ideas, in a way that no other medium has allowed before. However, the internet by itself can't connect people, there needs to be services built on top of it to do that, and that's why I was excited about the idea of social media when I first heard about it.

A simple example of how social media can bring people together in a way that was never done before is the Arab Spring, where people reported on things that media outlets either didn't know about or wasn't interested in covering.

I realized the power of social media during that time, and what attracted me most to them was the fact that unlike all other news organizations in the Middle East, it seemed that to me at the time that social media wasn't serving an ideology or an agenda or an entity, it was the voice of the people.

So I got into social media, and it was exciting. I have had so many experiences and chance encounters that would not have been possible without social media. These platforms do so much good in the world, just the fact that they remind you of friends' birthdays is amazing and the fact that they allow you to stay connected with people even after they change their phone numbers/move to another country/lose access to their email is wonderful. Social media platforms have pretty much made it impossible to lose track of someone's contact info.

However, with all the great things that social media does in the world, the negative effects of it far outweigh the positives, here's why:

* **USA**. Social media platforms are the biggest publishing house in history. Besides the Chinese social media platforms, which cater to Chinese people mostly, every major social media platform is American. Now, this issue is much much bigger than social media or the internet. However, its effects are more apparent in social media platforms because of their sheer power and immense reach. The problem with one country controlling the knowledge faucets of the world is that it gives that country (regardless of what country it is) relentless power over the rest of the world. As Foucault said, "We should admit rather that ... power and knowledge directly imply one another; that there is no power relation without the correlative constitution of a field of knowledge". Of course, this is a bigger issue than just social media.


* The business model of most social media platforms is built on "engagement" i.e. getting their users addicted. I truly am disgusted by this word, and by how normalized it is in the tech world. Jaron Lanier (who is amazing) wrote a book titled *ten arguments for deleting social media*, in it, he said, "engagement is not meant to serve any particular purpose other than its own enhancement, and yet the result is an unnatural global amplification of the "easy" emotions, which happen to be the negative ones".

    The way social media tries to get users addicted is by curating what they see. Which feeds 🐵 into a much bigger problem, because the internet is supposed to be free, it's supposed to be the place that people go to escape their bubbles, to get exposed to new ideas and views that they won't find in their own lives. Social media, creates a more dangerous for of intellectual isolation, the [filter bubble](https://en.wikipedia.org/wiki/Filter_bubble). The problem with this kind of thought isolation is that it gives users the illusion of being in an open internet, of interacting with human beings the same they interact with people in real life when in truth they are only interacting with people and stories that algorithms choose to make visible. And those algorithms main purpose is to get people more addicted.

    The approach of "If you like this, you will like that" is prominent all over the internet, but the effects of it are especially toxic in social media platforms. And the main issue with it is that it presents a false representation of society, thus deliberately giving users false information and nurturing an environment that promotes ignorance. This is done by only showing posts that users are likely to agree with. Moreover, this filtering is hidden and is done behind the scenes. All the meanwhile giving the user the illusion that the thoughts that are exposed to on social media platforms mirror those of society.

* **It's expensive**. Well, not in the typical sense of the word. Which is why this isn't talked about as much as it should. Let's start with the obvious, social media is not free. Yes, users won't be asked to pay $$$ to access a social media account. But, they will pay for it with their time and attention. Which is why social media is not free, it's just that we are not customers of social media, we are the product and the only reason they exist is to profit off of manipulating their users.

    I mean really, what are we, as people, what is there left of us, of our humanity, at the end of the day when social media is done eating up our attention.

* **They don't care about us** 🕺. Time and time again these companies have chosen to be greedy over looking after the interest of their users.

* **They own us**. Simply put, they do not enable you to use their platform without giving them your data.

* It's incredibly sad that of the world's best minds and talents, are working out better ways to serve ads

* It's also sad that for a platform as open and crucial to everyday life as the internet to be so restricted as that most data exists within a handful or less of silicon valley companies.

* For me personally, I think social media platforms are ugly and they have a boring design. The card-isque design of Facebook/Instagram/Twitter is holding the internet back. For tech companies that are the bleeding edge of technology, I expect more than stuck-in-2012-design looking products.

* Lastly, they are poised to dominate not only what media we consume, but also our means of digital communication. And that to me, is scary.

## Resources
* LARB's [review](https://lareviewofbooks.org/article/thieves-of-experience-how-google-and-facebook-corrupted-capitalism) of [The Age of Surveillance Capitalism](https://goodreads.com/author/show/710768.Shoshana_Zuboff).
* Jaron Lanier's book [Ten Arguments for Deleting Your Social Media Accounts Right Now](http://jaronlanier.com/tenarguments.html).
* Adam McLane's blog [In Social Media You Are the Product](https://adammclane.com/2013/03/in-social-media-you-are-the-product).
