---
layout: post
title: Social Media (Part II)
date: 2017-12-05
---

**Update 2019:** Social media has been getting a lot of negative coverage lately. And while I'm not a fan of social media (as you're about to read haha). I find it disgusting how normalized hate rhetoric is against [mark zuckerberg](https://www.bloomberg.com/features/2019-facebook-neverending-crisis). I don't agree with him on many things but still, I don't think this type of offensive language is ok nor is it productive.


This is a [follow up post](/why-i-dont-have-social-media) to a previous one that I wrote a year ago where I complain about social media :)

There are so many things that I like about social media. It keeps me connected with so many people that I love and care about. People that otherwise I would never have the time/energy to connect with.

However, it is also a double edged sword, there are a few points that I want to add to the previous post:

  * One thing I want to acknowledge is how useful social media is. And how it has made so many lives easier and better. And although I have in general negative sentiments towards these platforms. I also understand that the people that founded and run these companies and the people that work there aren't evil or bad by any means. They are business people and entrepreneurs. I think that the shortcomings of these platforms should be blamed on society and governments for failing to regulate these platforms, not their creators.


  * For many people it is an extremely toxic echo chamber. And while I think that most people have lived their lives before social media being only exposed to opinions that they agree with. Social media aggravates this problem, because these platforms sell themselves as spaces that connect people from all backgrounds and opinions, while that is far from the truth. They are curated by design to enforce tunnel visioning among users.

  * The illusion of checking up on friends/family/people in so many occasions isn't true. I like keeping up with friends and knowing where they are in life, even if we're too busy to talk. However, more often than not, I get the feeling that I am hearing way too much from certain people and way too little from certain people. Thus making social media a not very effective way to check up on people.

  * Privacy: I dislike that social media tracks users. And even more, I dislike that there exists a nonconsensual recorded history of me on social media.

  * I usually joke that I don't know how "new" tech. I get asked often why I don't use tech x or gadget y. Like for instance why my phone is a few years old and why it doesn't have data (I didn't own a smartphone until recently) and why I don't have any intrusive tech in my home. But the truth is that I don't have these things now because I don't know how they work or how to use them. I don't have them precisely because I know them.
