# Blog [![MIT license](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://gitlab.com/qirh/blog/raw/main/LICENSE)
[saleh.soy](https://saleh.soy)
## Copyright Information
  * ~~Powered by Gitlab pages~~ I switched to Netlify so I can have branch deploys :)
  * Forked from [Hugo](https://gitlab.com/pages/hugo)
  * Theme: [Beautiful Hugo](https://github.com/halogenica/beautifulhugo)
  * Repo icon: Icon made by madebyoliver from [flaticon](https://flaticon.com)
  * Blog icon: Icon made using [ionos](https://ionos.com/tools/favicon-generator)

This blog was migrated from [Github](https://github.com/qirh/qirh.github.io). The migration process was simple and quick, I love Gitlab!
