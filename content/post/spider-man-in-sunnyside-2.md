---
layout: post
title: Spider-Man in Sunnyside (Part 2)
date: 2023-09-08
---

Continuing from [part 1](https://saleh.soy/spider-man-in-sunnyside-part-1).

In this post, we'll go over:
* 2019's [Spider-Man: Far From Home](https://en.wikipedia.org/wiki/Spider-Man:_Far_From_Home)

Spider-Man travels a bunch in `Spider-Man: Far From Home`, so there is less to write about when compared to 2018's or 2021's films.

Before we start، a warning. This series will be **FILLED WITH SPOILERS** for Spider-Man movies and videogames.


## Spider-Man: Far From Home (2019)

* The Ramones are also featured in this film. The song in featured in the film is `I Wanna Be Your Boyfriend` ([youtube](https://www.youtube.com/watch?v=BDvtkIp8UU4)). They're also featured in the [teaser trailer](https://www.youtube.com/watch?v=DYYtuKyMtY8) with their song `I Wanna Be Sedated`.

The Ramones-Spider-Man love goes both ways though. The Ramones released a song in 1995 called Spider-Man ([youtube](https://www.youtube.com/watch?v=i3F2y2hRP4o)) & ([wikipedia](https://en.wikipedia.org/wiki/Spider-Man_(theme_song)#:~:text=In%201995%2C%20Ramones%20recorded%20a,the%20hyphen%20as%20%22Spider-man%22.)).

<div class="linebreak"></div>

* In the beginning of the movie, in the art class scene. We see a picture of "The Unicorn Rests in a Garden". I don't know what the other pieces of art are (if you know lmk). But this beautiful tapestry hangs in the Met Cloisters in Uptown Manhattan. [link](https://www.metmuseum.org/art/collection/search/467642). There is actually a recreation of it in Stirling castle in Scotland [link](https://www.stirlingcastle.scot/discover/highlights/the-stirling-tapestries/)

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_unicorn.jpeg" style="width:80%">


<div class="linebreak"></div>

* Soon after. Aunt May is holding a fundraiser at an "Urban Sports * Cultural Center". 
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_cultural_center.jpeg" style="width:80%">
    
This is actually Prudential Savings Bank on the corner of Stuyvesant and Vernon avenues on 400 Vernon Ave, Brooklyn in the border between the neighborhoods of Bed-Stuy & Bushwick. Even though this is in Brooklyn, it's really close to Sunnyside (where Peter and his aunt live in this universe). And so it's totally possible that they are involved in community things over there. 

You can see in this [Google Street View](https://www.google.com/maps/@40.6963754,-73.9345812,3a,44.5y,150.75h,102.74t/data=!3m6!1e1!3m4!1sqlkQ03zi-rkIeJUiniKvtg!2e0!7i16384!8i8192?entry=ttu
) photo the original structure. It's kind of funny that they CGI'd the sign. I wonder if it's for some trademark reason.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_cultural_center_actual.jpeg" style="width:80%">

The train line in the background is the J/M/Z elevated train line. Even though the scene doesn't show it the Myrtle station is just beyond that corner. This is the J/M/Z Myrtle Ave stop, not to be confused with the Myrtle–Wyckoff Avenues stop on the L train just down the street lol. Just remember this meme I stole from the internet whenever you get confused between the L line and the J/M/Z.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_cultural_center_train.webp" style="width:80%">

Back to the bank building. The architects of this bank were [Rudolph Daus](https://www.brownstoner.com/architecture/building-of-the-day-5-stuyvesant-avenue/) & [Carl L. Otto](https://www.nytimes.com/1972/06/21/archives/i-carl-otto-designed-brooklyn-buildings.html). The pre-war era saw the construction of many spectacular banks. this is a fine example of that period. Though it is now empty and desolate, which many historic buildings are in NYC unfortunately. Such a shame, if I was in charge these buildings would truly serve the community instead of being empty and being allowed to fall into ruin. You can cite this blog when I finally run for "the person in charge".
    
Daus & Otto designed multiple buildings in the NYC metro area. One of my favorites is the Church of Notre Dame in the neighborhood of Morningside in Manhattan.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_cultural_center_notre_dame.jpeg" style="width:80%">

It is truly a sanctuary of peace and tranquility from the business of the city. And it is such a hidden gem. the above picture I took in April 30, 2021.

<div class="linebreak"></div>

* We then see Spider-Man get flustered and escape the fundraiser. 
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_rooftop_a.jpeg" style="width:80%">

This is the rooftop of the Prudential Savings Bank [Google Maps](https://www.google.com/maps/place/400+Vernon+Ave,+Brooklyn,+NY+11206/@40.6956523,-73.9346928,67a,35y,46.6t/data=!3m1!1e3!4m6!3m5!1s0x89c25c09cabc7697:0xd23094617ba8b329!8m2!3d40.6962116!4d-73.9345639!16s%2Fg%2F11h8b4k562!5m1!1e1?entry=ttu).

This scene looks like a green screen to me. But even though, I'm really impressed with how the locations team has been putting effort to depict NYC and the accuracy of it all. Great job !!

Then Spider-Man turns and faces the city (NYC speak for Manhattan). That view looks quite plausible from where the bank is. Behind Spider-Man we see lettering on the building. I wonder if that's part of the Grafitti that's on the building?

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_rooftop_b.jpeg" style="width:80%">

<div class="linebreak"></div>

* Waaay later in the film, in one of Mysterio's trick illusions. We see this sign that says Queens. 
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_sign.jpeg" style="width:80%">

This is actually the Sunnyside sign. Another great ode from the locations team!! It's funny they changed it from Sunnyside to Queens. This is actually the first time I see the sign on film, ironic that it had to be a marvel movie that shows bits of the local culture over here haha.

This is the sign as seen from the 7 train platform. (46th St station).
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_sign_actual_a.jpeg" style="width:80%">

And this is the sign as seen from 46th St facing the train line. I love this sign, it's so beautiful. But just like most things in NYC, it's in dire need of some TLC.
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_sign_actual_b.jpeg" style="width:80%">

<div class="linebreak"></div>

* Towards the end of the film, we see Spider-Man and friends come back to NYC and they land in Newark. Now anyone who knows anyone whose been to Newark knows it doesn't look like that. Not even when they build it, it didn't look that clean. This is Stansted Airport in London [link](https://www.bishopsstortfordindependent.co.uk/news/stansted-airport-adds-spider-man-far-from-home-to-film-credits-9075211/).
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_newark_inside.jpeg" style="width:80%">

Then Spider-Man heads outside and sees Aunt May.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_newark_outside.jpeg" style="width:80%">
    
A few things wrong with this.
    1. The background is a green screen. Newark sure don't look nice on the inside and they keep it pretty consistent with the outside too.
    2. Newark airport, like all the other airports and sea ports (within 25 miles of the Statue of Liberty) is controlled by the Port Authority of New York and New Jersey. And so the cops with be Port Authority cops not Newark PD. Fun fact the Port Authority has won the award for "worst design" of flag every year since its creation. Just look at the flag, it's the NY and NJ state seals together lol.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_port_authority_flag.png" style="width:80%">
    3. nit: but the Newark/Manhattan bus is yellow, not blue

Lastly, It kind of sucks they didn't use LaGuardia Airport since it's 10 minutes from Sunnyside. It actually looks pretty nice now. But they didn't finish renovations until 2021.

<div class="linebreak"></div>

* We then see Spider-Man swing around in Midtown. This is looking down on Broadway at the intersection with 37th st.
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_midtown.jpeg" style="width:80%">

This is what it looks like on [Google Street View](https://www.google.com/maps/@40.7520616,-73.9875441,3a,23.1y,183.98h,97.29t/data=!3m7!1e1!3m5!1sG7CeRwPeLwF7OyiONfT0YQ!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DG7CeRwPeLwF7OyiONfT0YQ%26cb_client%3Dsearch.revgeo_and_fetch.gps%26w%3D96%26h%3D64%26yaw%3D233.72676%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?entry=ttu).

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_midtown_actual.jpeg" style="width:80%">

The building being built in the background I think is The Continental which was built 2008-2010 so it doesn't make sense that it's still being built in this shot. I've looked at this [map](https://developers.arcgis.com/javascript/latest/sample-code/visualization-vv-color-animate/live/index.html), and can't find any skyscrapers that look like that one that were under construction around that time.
    
<div class="linebreak"></div>

* We then see Spider-Man swinging over a glorious-looking building with a green copper looking roof. Unfortunately, I don't think this is a real building, which makes sense since some buildings have trademarks. I know of two skyscrapers that have roofs like that (copper and pointy). [The Woolworth Building](https://en.wikipedia.org/wiki/Woolworth_Building) & [40 Wall St](https://en.wikipedia.org/wiki/40_Wall_Street). It looks to me closer to the former. Actually it's for [sale](https://galeriemagazine.com/de-beers-high-jewelry/). So if anyone wants to pitch in I'm down :)

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_skyscraper_green_roof_a.jpeg" style="width:80%">

In the second picture, we see the beautiful crown of the building. Which doesn't match any of the former two buildings. But actually looks like the crown of the [Manhattan Municipal Building](https://en.wikipedia.org/wiki/Manhattan_Municipal_Building). I love this [video](https://vimeo.com/456405943) which was filmed up there, gosh I wish I could go up there!

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_skyscraper_green_roof_b.jpeg" style="width:80%">

One last note about these 3 buildings. They are all downtown. When Spider-Man is swinging in Midtown. Knowing how the filmmakers are trying to stay true to the city and its geography. I think it's because of how beautiful these buildings are that they decided to go with them. 

<div class="linebreak"></div>

* Right after, Spider-Man flies through the empty center of this futuristic-looking building. I wish this building existed but it don't. This is located where the [Pan Am Building](https://en.wikipedia.org/wiki/MetLife_Building) aka MetLife is. Right behind Grand Central Terminal. Honestly, the building looks like they could carve out the middle of it and make this a reality.
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_skyscraper_futuristic.jpeg" style="width:80%">

A [helicopter accident](https://www.nytimes.com/1977/05/17/archives/5-killed-as-copter-on-pan-am-building-throws-rotor-blade-one-victim.html) on the rooftop of this building in 1977 led to the ban of private helicopters landing in NYC rooftops.

Interestingly in the past few years (2020s) helicopters flying over NYC have come to the spotlight again. In Dec of 2022 NY Govener Hochul vetoed a bill ([Stop the Chop Act](https://www.nysenate.gov/newsroom/press-releases/2022/brad-hoylman-sigal/statement-senator-hoylman-veto-stop-chop-act)) that banned tourist helicopters from flying over NYC. AS a person that lived next to the Hudson River (West Side of Manhattan). I can attest to how freaking annoying and noisy having helicopters fly over your home all day long.

<div class="linebreak"></div>

* This last scene (post-credit) is where this movie ends. And it's also the first scene of 2021's `Spider-Man: No Way Home`. Which we'll go over in the next blog. This is on 8th Ave and 33rd St. Between Penn Station/MSG (where the screens are) and the new Moynihan Train Hall.
<img src="https://gitlab.com/qirh/blog/-/raw/main/static/spiderman/2019_penn_station.jpeg" style="width:80%">

<div class="linebreak"></div>

This is the end of part 2 :)