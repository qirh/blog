---
layout: post
title: Living in a Small Space
date: 2018-02-28
---

My room in NYC is quite small, definitely one of the smallest space that I've ever lived in. Well, I've lived in a net smaller area per person (larger room but more people living in it). Which I feel gives more space to everyone since we shared the room essential things (stationeries, table, ...) and thus we collectively had more space. But living in a space this small is new to me. In general, it's been fun and hasn't bothered me. Especially that I haven't been staying home much since I moved to the city. I definitely realize now more than anytime that making it work in such a small place requires skill and thought.

Here are some ups and downs that I noticed so far.

**Ups:**
1. I save money but not buying things that I don't need anymore. There is simply nowhere to put anything. And so I have to justify every new object that I introduce to my life because I know it's going to be a headache later on trying to find space for it to live. I'd say probably I'm somewhere on the minimalism scale, I don't like collecting or owning things, but even by my standards, this is another level of minimalism ha!
2. It's easy to keep clean.

**Downs:**
1. It's hard to keep tidy and organized because there is so little space.
2. It's really hard to stretch or do any sort of body movement because there is no space unless I'm vertical or in bed. For example, it's hard to pray. Illustriom of prayer:

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/prayer.webp" style="width:80%">
