---
layout: post
title: News Letters (where I get my news)
date: 2022-04-01
---

I get most of my news from newsletters. I really like newsletters. I go through my email inbox 1-3 times a day and mostly skim the headlines. I'd like to read everything, but I receive way too many emails. And have to be picky about what I spend my time reading.

I subscribe to a lot of publications that I don't agree with a lot of their views. But I still like to informed.

I started an effort (mostly for my sake) to collect a list of all the newsletters that I receive. I think I got most of them but I'm sure there are some that did not send anything in the time frame that I was collecting this information.


[My favorite podcasts/blogs/newsletters](https://noiseless-flame-9a6.notion.site/Periodicals-Podcasts-that-I-subscribe-to-0473283deae642e1a24b419812b828d6).