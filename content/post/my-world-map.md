---
layout: post
title: My World Map
date: 2020-08-10
---

Hello, this is something I've been wanting to do for a long time! A few years ago I saw a map on the internet that someone made of the countries that they speak the language of. I unfortunately don't remember where the original was from, I only saved the tool they used to create their map. It's a website called [mapchart](https://mapchart.net/world.html). Here is the [json file](https://gitlab.com/qirh/blog/-/raw/main/static/map_data.json) I used to create this map.

I created two maps. The first is [without borders](https://youtube.com/watch?v=BaSb5smP-dM) and is my favourite. The second one has borders.

I'm familiar a decent amount with the Middle East, North Africa, Western Europe and the Americas. There's so so much left of the world to see and explore! I've been thinking a lot about it, and in the next two years I would like to learn more about and visit the balkans and the caucasus.



  <div style="margin-bottom:10px;margin-top:10px;">
    <img src="https://gitlab.com/qirh/blog/-/raw/main/static/map_without_borders.png" alt="world map without borders">
  </div>
  <div>
    <img src="https://gitlab.com/qirh/blog/-/raw/main/static/map_with_borders.png" alt="world map with borders">
  </div>
