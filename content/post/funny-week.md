---
layout: post
title: Funny Week
date: 2023-05-21
---

I had a bit of a funny week. I went to 3 concerts. And I ran two half-marathons, both of which was followed with helping a friend move. And one night I got to leave NYC (even though not far, I only went to Westchester).

The concerts I went to were all from the MENA region:
1. On Monday I went to a Palestinian concert at the General Assembly of the UN. It was amazing to be in that space, and the music was great ([link](https://www.un.org/unispal/event/nakba75/)). The New York Arabic Orchestra, which performed at his concert has an annual concert in December which is one of my fav things to do in winter <3
2. On Wednesday I went to an Andalusian concert at La Nacional. It was so incredible, I loved it so much and look forward to their future events ([link](https://www.eventbrite.com/e/ny-andalus-ensemble-spring-2023-tajdid-renewal-tickets-620225650617)).
3. On Friday I went to a Syrian concert at Carnegie Hall. This concert was by Takht al-Nagham, which I've seen once before and they did incredible too, just as I expected ([link](https://www.carnegiehall.org/Calendar/2023/05/19/Love-and-Loss-Traditional-Music-of-Syria-0800PM)).


And I ran two half marathons:
1. On Tuesday, just by myself I ran through 4 boros (Queens -> Brooklyn -> Manhattan -> Bronx) + Randalls Island ([strava](https://www.strava.com/activities/9085141469)). Then I helped my friend move a bed.
2. On Saturday I ran the RBC Brooklyn half ([strava](https://www.strava.com/activities/9106292275)). Then I helped another friend move his entire apt + storage lol <-- this was tough.


And on Sunday, I'm writing this and catching up with errands + chores + getting ready for my upcoming travel + continuing the move from yesterday (we still have a few things we didn't finish).

Anyways this is a high level view of my week, it's not usual to have a week this busy in NYC. But it's also not rare, I'd say it happens once every 4-6 weeks. It's a big part of why I love NYC. I definitely love being active and moving around. 

Funnily enough, I started reading 4000 weeks this weeks and in the intro/1st chapter all the author talks about is how we're addicted to being busy and value productivity too highly. I agree with the author in some parts. But, I don't see myself like that, because I love the quiet times just as much as the busy ones.

<img src="https://gitlab.com/qirh/blog/-/raw/main/static/i-heart-ny.gif" alt="I <3 NY" style="width:200px">