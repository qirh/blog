#### Hey welcome to my blog, I'm [Saleh](http://saleh.sh)!

Thank you for being here, it means a lot to me. I come from a long line of human beings and I'm fairly certain I'm the first one out of all of em to have a blog, so this is a big deal! 

This website is not indexed by search engines, so if I didn't send you it, I'd love to know how you got here :)

I'd also love to hear any feedback or ideas or anything that you have to share. Or if you would like to just say hi, that would make me very happy, my email is at the bottom.
